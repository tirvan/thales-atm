# Thales Atm

The code is written in Python and using Flask library which later package into Kubernetes Pod

**Dockerfile**

Using Python 3.6 and exposing port 3000

**requirements.txt**

Contains library needed to build the docker image

**thalesnodeport.yml**

Exposing port 32021 for web access

**thales-pod.yml**

Running the container at port 3000 and using host network

**thales.py**

The main code to find top waypoints that are associated with the most number of SIDs and STARs

_References:_

https://betterprogramming.pub/getting-started-with-kubernetes-for-python-254d4c1d2041

https://www.tecmint.com/install-a-kubernetes-cluster-on-centos-8/
